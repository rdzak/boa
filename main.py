# Given a words.txt file containing a newline-delimited list of dictionary
# words, please implement the Anagrams class so that the get_anagrams() method
# returns all anagrams from words.txt for a given word.
#
# Bonus requirements:
#   - Optimise the code for fast retrieval
#   - Write more tests
#   - Thread safe implementation

import unittest
from itertools import permutations
from bisect import bisect_left
from collections import defaultdict
import threading


class Anagrams:

    """Implements three algorithms of anagram lookup. Defaults to the fastest one. Includes threaded version.
        To test with longer word use best algorithm"""

    def __init__(self, method='dict'):
        """To save memory file is loaded into only one of data structures, depending of method choice"""
        self.method = method
        with open('words.txt') as f:
            self.words = {'set': set, 'list': sorted, 'dict': self.__prebuild}[self.method](f.read().splitlines())

    def get_anagrams(self, word=''):
        """Returns all anagrams that exist in file for given word using method of choice"""
        try:
            word = word.replace(' ', '').lower().strip()
        except AttributeError:
            return []
        return {'set': self.__find_in_set, 'list': self.__find_in_list, 'dict': self.__find_in_dict}[self.method](word)

    def __prebuild(self, words):
        """Returns dictionary of anagrams. Keys are alphabetically sorted words. Values are lists of matched anagrams"""
        anagrams_map = defaultdict(set)
        for word in words:
            anagrams_map[''.join(sorted(word))].add(word)
        return anagrams_map

    def __find_in_dict(self, word):
        """Returns matching anagrams using lookup in prebuild dictionary"""
        return sorted(list(self.words[''.join(sorted(word))]))

    def __find_in_set(self, word):
        """Returns matching anagrams using set lookup"""
        return sorted([perm for perm in self.__permutate(word) if perm in self.words])

    def __find_in_list(self, word):
        """Returns matching anagrams using binary search"""
        return sorted([perm for perm in self.__permutate(word) if self.__exists(self.words, perm)])

    def __permutate(self, word):
        """Returns all permutations of a word"""
        return set([''.join(perm) for perm in permutations(word)])

    def __exists(self, lst, item):
        """Returns True if value found in list, using binary search, False otherwise"""
        length = len(lst)
        index = bisect_left(lst, item, 0, length)
        return lst[index] == item and index != length

    def find_anagrams_threaded(self, words):
        """Returns anagrams for each word spawning one thread per each"""
        from multiprocessing.dummy import Pool as ThreadPool
        pool = ThreadPool(len(words))
        results = pool.map(self.get_anagrams, words)
        pool.close()
        pool.join()
        return results


class TestAnagrams(unittest.TestCase):

    cases = {
        'plates': ['palest', 'pastel', 'petals', 'plates', 'staple'],
        'eat': ['ate', 'eat', 'tea'],
        'anagram': ['anagram'],
        'Nag a Ram': ['anagram'],
        'xxx': [],
        # 'wordthatisevenlongerbutnotthere': [], # use test_anagrams_dict test as the most effective
        '': [],
        None: [],
        False: [],
        0: [],
        '': []
    }

    def test_anagrams_list(self):
        anagrams = Anagrams(method='list')
        for k, v in self.cases.items():
            self.assertEquals(anagrams.get_anagrams(k), v)

    def test_anagrams_set(self):
        anagrams = Anagrams(method='set')
        for k, v in self.cases.items():
            self.assertEquals(anagrams.get_anagrams(k), v)

    def test_anagrams_dict(self):
        anagrams = Anagrams(method='dict')
        for k, v in self.cases.items():
            self.assertEquals(anagrams.get_anagrams(k), v)

    def test_anagrams_threaded_using_all_methods(self):
        for method in ['set', 'list', 'dict']:
            anagrams = Anagrams(method=method)
            results = anagrams.find_anagrams_threaded(list(self.cases.keys()))
            self.assertEquals(sorted(results), sorted(list(self.cases.values())))


if __name__ == '__main__':
    unittest.main()
